# -*- coding: utf-8 -*-

{
    'name': 'Webpay Payment Acquirer',
    'category': 'Payment / Chile',
    'author': 'Daniel Santibáñez Polanco',
    'summary': 'Payment Acquirer: Chilean Webpay Implementation',
    'website': 'https://globalresponse.cl',
    'version': "2.0.0",
    'description': """Chilean Webpay Payment Acquirer""",
    'depends': [
                'payment',
                'payment_currency',
                'sale_payment',
                'website_sale',
            ],
    'external_dependencies': {
        'python': [
            'suds',
            'urllib3',
            'transbank',
        ],
    },
    'data': [
        'views/webpay.xml',
        'views/payment_acquirer.xml',
        'views/payment_transaction.xml',
        'views/website_sale_template.xml',
        'data/webpay.xml',
    ],
    'installable': True,
    'application': True,
}
